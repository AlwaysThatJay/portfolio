import { Component, Renderer2, OnInit } from '@angular/core';

@Component({
  selector: 'app-homescreen',
  templateUrl: './homescreen.component.html',
  styleUrls: ['./homescreen.component.scss']
})

export class HomescreenComponent implements OnInit {

  title = 'portfolio';
  config: any;

  // tslint:disable-next-line: variable-name
  fullpage_api: any;

  constructor(private renderer: Renderer2) {
    // for more details on config options please visit fullPage.js docs
    this.config = {

      // fullpage options
      responsiveWidth: 900,
      anchors: ['page1', 'page2', 'page3', 'page4', 'page5'],
      menu: '#menu',


      // fullpage callbacks

      // afterResponsive: (isResponsive) => {
      // },
      // afterResize: () => {
      //   console.log('After resize');
      // },
      // afterLoad: (origin, destination, direction) => {
      //   console.log(origin.index);
      // }
    };
  }

  ngOnInit() {
  }


  getRef(fullPageRef) {
    this.fullpage_api = fullPageRef;
  }

}


