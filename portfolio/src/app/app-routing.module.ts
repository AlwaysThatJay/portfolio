import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoComponent } from './info/info.component';
import { AppComponent } from './app.component';
import { HomescreenComponent } from './homescreen/homescreen.component';

const routes: Routes = [
  {path: '', component: HomescreenComponent, pathMatch: 'full' },
  {path: 'home', component: HomescreenComponent},
  {path: 'info', component: InfoComponent},
  {path: '*', component: HomescreenComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
